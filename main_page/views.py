from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from posts.models import BlogPosts, Cases


def contacts(request):
    context = {
        'name': 'Contacts',
        'title': 'Раздел Контакты'
    }
    return render(request, 'main_page/contacts/contacts.html', context)


def all_data():
    posts = BlogPosts.objects.all()[:3]
    cases = Cases.objects.all()[:3]

    context = {
        'title': 'Главная',
        'posts': posts,
        'cases': cases
    }
    return context


def index(request):
    return render(request, 'main_page/index.html', all_data())
