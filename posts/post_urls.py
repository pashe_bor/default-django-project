from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.post_preview_view, name='index'),
    url(r'^(?P<id>\d+)/$', views.cases_detail_view, name='detail'),
]
