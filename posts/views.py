from django.shortcuts import render
from .models import Cases, BlogPosts


def get_cases():
    all_cases = Cases.objects.all()

    context = {
        'name': 'Contacts',
        'title': 'Все кейсы',
        'cases': all_cases
    }
    return context


def get_all_posts():
    all_posts = BlogPosts.objects.all()

    context = {
        'name': 'Блог',
        'title': 'Блог',
        'posts': all_posts
    }
    return context


def cases_preview_view(request):
    return render(request, 'posts/cases/index.html', get_cases())


def cases_detail_view(request, id):
    single_case = Cases.objects.get(id=id)
    return render(request, 'posts/cases/detail_cases.html', {'title': single_case.case_name, 'case': single_case})


def post_preview_view(request):
    return render(request, 'posts/articles/preview.html', get_all_posts())
