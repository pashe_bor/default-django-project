from django.conf import settings
from django.db import models
from datetime import datetime
from django.core.files.storage import FileSystemStorage

private_storage = FileSystemStorage(location=settings.PRIVATE_STORAGE_ROOT)


class BlogPosts(models.Model):
    title = models.CharField(max_length=200)
    watches = models.CharField(max_length=1000, default=1)
    desc = models.CharField(max_length=200)
    keywords = models.CharField(max_length=200, default=200)
    body = models.TextField()
    date_created = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Статьи"


class Cases(models.Model):
    case_name = models.CharField(max_length=200)
    image = models.FileField(storage=private_storage)
    keywords_meta = models.CharField(max_length=200, default=200)
    description_meta = models.CharField(max_length=200)
    detail_text = models.TextField()
    preview_text = models.TextField()
    date_created = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.case_name

    class Meta:
        verbose_name_plural = "Кейсы"
