from django.contrib import admin
from .models import BlogPosts, Cases

admin.site.register(BlogPosts)
admin.site.register(Cases)
