# Generated by Django 2.0.7 on 2018-07-27 13:25

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0003_blogposts_watches'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cases',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('case_name', models.CharField(max_length=200)),
                ('image', models.FileField(upload_to='')),
                ('keywords_meta', models.CharField(default=200, max_length=200)),
                ('description_meta', models.CharField(max_length=200)),
                ('detail_text', models.TextField()),
                ('preview_text', models.TextField()),
                ('date_created', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
    ]
